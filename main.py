import cv2
from pathlib import Path
import argparse
from lpc_si import lpc_si
import numpy as np
import random
from tqdm import tqdm

IMG_EXTENSIONS = [
    ".jpg",
    ".JPG",
    ".jpeg",
    ".JPEG",
    ".png",
    ".PNG",
    ".ppm",
    ".PPM",
    ".bmp",
    ".BMP",
    ".tif",
]


def is_image_file(filename):
    return any(filename.suffix == extension for extension in IMG_EXTENSIONS)


def main(args):
    if args.seed:
        print("Set random seed to", str(args.seed))
        np.random.seed(args.seed)

    path = Path(args.source_dir)
    verbose = args.verbose

    imgs = list(path.glob("*"))
    imgs = list(filter(is_image_file, imgs))

    if args.nb_sample and args.nb_sample < len(imgs):
        print("Compute LPC-SI for", args.nb_sample, "samples")
        imgs = random.sample(imgs, args.nb_sample)

    res = 0
    maps = []
    for img_path in tqdm(imgs, total=len(imgs)):
        img = cv2.imread(str(img_path)).astype(np.float32)
        # convert BGR to grayscale
        img = 0.2989 * img[:, :, 2] + 0.5870 * img[:, :, 1] + 0.1140 * img[:, :, 0]
        LPC_SI, lpc_map = lpc_si(img)
        res += LPC_SI
        if verbose:
            print(str(img_path.name), str(LPC_SI))
        maps.append(lpc_map)

    return res / len(imgs), maps


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("source_dir")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument(
        "--nb_sample", type=int, help="Take a number of random samples of images."
    )
    parser.add_argument("--seed", type=int)
    args = parser.parse_args()
    res, maps = main(args)
    print("mean LPC-SI", res)

Python implementation of the LPC-SI metric

Tested with Python 3.8

```bibtex
@article{hassen2013image,
  title={Image sharpness assessment based on local phase coherence},
  author={Hassen, Rania and Wang, Zhou and Salama, Magdy MA},
  journal={IEEE Transactions on Image Processing},
  volume={22},
  number={7},
  pages={2798--2810},
  year={2013},
  publisher={IEEE},
  url = {http://ieeexplore.ieee.org/document/6476013/}
}
```
